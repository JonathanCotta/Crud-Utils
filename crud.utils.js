(function ($) {
    var idSelecionado = null;

    $.fn.actionLink = function (ent, token) {
        getId();
        $(this).on("click", function () { // attach event onclick
            if (idSelecionado == null) {
                alert("Selecione um registro")
            }
            else {
                var action = this.getAttribute("id");
                if (action == "Delete") {
                    actionDelete(ent, idSelecionado, token); // send ajax request to delete the selected
                }
                else {
                    window.location.href = "/" + ent + "/" + action + "/" + idSelecionado; // go to edit or show page
                }
            }
        });
    }

    var getId = function () { // get the entity id  from the first table column id, in the tbody
        $("tbody").on('click', 'tr', function () {
            $("tr").removeClass("selecionado");
            $(this).toggleClass("selecionado");
            linha = $(this).children("td");
            idSelecionado = linha.eq(0).prop("id");
        });
    };

    var actionDelete = function (ent, id, token) {
        var url = "/" + ent + "/Delete/";
        $.post(url, { id: id, __RequestVerificationToken: token })
        .done(function (data) {
            if (data.success) {
                alert("Registro removido com sucesso"); // success message
                window.location.href = "/" + ent + "/Index"; // after delete go to index page
            }
            else {
                alert(data.msg);
            }
        })
        .fail(function () { alert("Erro no envio da requisição") }); // fail message
    };

})(jQuery);
